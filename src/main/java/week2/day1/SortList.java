package week2.day1;

import java.util.ArrayList;

import java.util.TreeSet;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class SortList {

	public static void main(String[] args) {
		
		List<String> ls=new ArrayList();
		ls.add("in195");
		ls.add("in193");
		ls.add("in500");
		ls.add("in195");
		ls.add("in75");
		ls.add("in45");
		ls.add("in89");
		
		List<Integer> ls2=new ArrayList();
		ls2.add(15);
		ls2.add(193);
		ls2.add(50);
		ls2.add(1);
		
		System.out.println(ls2);
		
	System.out.println("List of employeesid with sorting order is:");
		
		Collections.sort(ls);
	    System.out.println(ls);
	    
	    //To remove duplicates and print in sorted order
	    
	    Set<String> st=new TreeSet<>();
	
	    System.out.println("List without duplicates are:");
	    st.addAll(ls);
	    System.out.println(st);
	

		
		
//We cannot sort a list using array concept


	/*	
		int size=ls.size();
		System.out.println(size);
		
		int temp;
		
			
		
		for(int i=0;i<size-1;i++)
		{
			int temp1=ls.get(i);
			for(int j=0;j<i+1;j++)
			{
				 int temp2=ls.get(j);
				if(ls.get(i)<ls.get(j))
					
				{	    
					temp=temp1;
					temp1=temp2;
					temp2=temp;			
				}				
			}
		
			
		}
		for(int k:ls)
		System.out.println(ls.get(k))
		;
	
*/
	}

}