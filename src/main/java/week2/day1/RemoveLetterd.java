package week2.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class RemoveLetterd {

	public static void main(String[] args) {
	
		//List of employees and remove name contains letter d and print in sorted order
		
        System.out.println("Enter the no of employees:");
        
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        
		System.out.println("Enter the name of employees:");
		List<String> ls=new ArrayList<>();
		
		for(int i=0;i<=n-1;i++)
		{
			ls.add(scan.next());
		}
		
		for(int j=0;j<ls.size();)
			{
		  if(ls.get(j).contains("d"))
		  
			  ls.remove(j);
		  else
			  j++;
		}
			
		System.out.println("List after removing names contain d is:");
		
		System.out.println(ls);
		
		System.out.println("Sorted order is:");
		
		Set<String> str=new TreeSet<>();
		str.addAll(ls);
		System.out.println(str);
		
		scan.close();	
	}

}
