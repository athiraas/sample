package week2.day1;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		List<String> ls=new ArrayList();
		ls.add("9995346443");
		ls.add("5896321459");
		ls.add("7894569821");
		ls.add("1234569872");
		ls.add("1234569872");
		
		System.out.println(ls.size());
		
		Set<String> set=new HashSet<>();
		
		
		//Converting list to set
		
	        set.addAll(ls);
		    
	        System.out.println("The new set elements without duplicates are:");
		    for(String str:set)
		     System.out.println(str);
		    
		    int size=set.size();
		    
		    
		    
		    	
		   //verification
		    int count=0;
		    
		
		 	for(int i=0;i<=size-1;i++)
		 	{
		 		for(int j=i+1;j<size;j++) {
		 			
		 			if(ls.get(i)==ls.get(j))
		 			
		 				count++;	 				 			
		 		}
		 		
		 	}
		    
		 	if(count!=0)
		 		System.out.println("No duplicates present in list");
		 	else
		 		System.out.println("Duplicates are present in list");
		 	
		 	
		 	
		 	
		 	for(String each:set) {
		 		
		 		int frequency=Collections.frequency(ls, each);
		 		System.out.println("value"   +each   +"occures" +frequency   +"times");	
		 			
		 	} 	
		 	
		 	
		 	
		    
	}
}



/*System.out.println("Verification");
int count=0;
//int size=set.size();
for(int i=0;i<size;i++)
{
	for(int j=0;j<i;j++)
	{	
		if(ls.get(i)==ls.get(j))
		{
			count=1;
	
		}
		
	}
}
if(count!=1)
	System.out.println("No duplicates: Verified");

	}

}
*/