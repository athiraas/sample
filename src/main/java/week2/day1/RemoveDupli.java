package week2.day1;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class RemoveDupli {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		  
		System.out.println("Enter the String with duplicates:");
		
		Scanner scan=new Scanner(System.in);
		String str=scan.next();
		Set<Character> set=new LinkedHashSet<>();
		char[] ch=new char[100];
		ch=str.toCharArray();
		
		for(char c:ch)
		{
			set.add(c);
		}
		
        System.out.println("The new set elements without duplicates are:");
	     System.out.println(set);	    
	  
	}

}
