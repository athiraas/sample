package week2.day2;

import java.util.Scanner;

public class DivisibleHw {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter the two numbers as input:");
		
		Scanner scan=new Scanner(System.in);
		int n1=scan.nextInt();
		int n2=scan.nextInt();
		
		for(int i=n1;i<=n2;i++)
		{
			
			if(i%3==0&&i%5==0)
				System.out.print(" FIZZBUZZ");
			else if(i%3==0)
				System.out.print(" FIZZ");
			else if(i%5==0)
				System.out.print(" BUZZ");
			else 
				System.out.print(i);					
		}
						

	}

}
