package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestleafMerge {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		
		WebElement eleUsername = driver.findElementById("username");
		eleUsername.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
        driver.findElementByLinkText("CRM/SFA").click();
        driver.findElementByLinkText("Leads").click();
        driver.findElementByLinkText("Merge Leads").click();
        driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following::img[1]").click();
		Set<String> Uniquenum = driver.getWindowHandles();
		List<String> Uninum = new ArrayList<>();
		Uninum.addAll(Uniquenum);
		
        driver.switchTo().window(Uninum.get(1));
        
        driver.findElementByXPath("//input[@name='id']").sendKeys("10079");
        driver.findElementByXPath("//button[text()='Find Leads']").click();
        Thread.sleep(2000);
        
        driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a").click();
        driver.switchTo().window(Uninum.get(0));
        driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following::img[2]").click();
        
        Set<String> Uniquenum1 = driver.getWindowHandles();
		List<String> Uninum1 = new ArrayList<>();
		Uninum1.addAll(Uniquenum1);
		
		     driver.switchTo().window(Uninum1.get(1));
		
		   driver.findElementByXPath("//input[@name='id']").sendKeys("10083");
		   
		   driver.findElementByXPath("//button[text()='Find Leads']").click();
		   
		   Thread.sleep(2000);
		   
		   driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a").click();
		   
		   driver.switchTo().window(Uninum.get(0));
		   
		   driver.findElementByLinkText("Merge").click();
		   
		   driver.switchTo().alert().accept();
		   
		   driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
		   
		   driver.findElementByXPath("//input[@name='id']").sendKeys("10079");
		   
		   driver.findElementByXPath("//button[text()='Find Leads']").click();
		   
		   driver.quit();
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		
		
        
        
        
        
        
        
        
		

	}

}
