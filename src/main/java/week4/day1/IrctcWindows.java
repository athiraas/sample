package week4.day1;

import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcWindows {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().timeouts().pageLoadTimeout(3000,TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
	    driver.manage().window().maximize();
	    driver.findElementByLinkText("Contact Us").click();
	    
	     //To get unique generated number
	    Set<String> Uniquesnum = driver.getWindowHandles();
	    List<String> listofwindows=new ArrayList<>();
	    
	    //converting to list for using get method
	    listofwindows.addAll(Uniquesnum);
	    
	    //switch to  1st index from that unique numbers
	    driver.switchTo().window(listofwindows.get(1));
	    //get title of that window
	    System.out.println(driver.getTitle());
	    
	    driver.quit();
	    
		
	}

}
