package week4.day1;

import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertandFrame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		
		driver.switchTo().alert().sendKeys("Athira");
	  
	   driver.switchTo().alert().accept();
	   String text= driver.findElementByXPath("//p[@id='demo']").getText();
	   
	   if(text.contains("Athira"))
	   
	   {
		   System.out.println("Element present");
	   }
		
		
		
		
		

	}

}
