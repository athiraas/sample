package week.day1;

import java.util.Scanner;

public class SwappingNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		System.out.println("Enter the two numbers a and b:");
		
		Scanner scan=new Scanner(System.in);
		int a=scan.nextInt();
		int b=scan.nextInt();
		
		a=a+b;
		b=a-b;
		a=a-b;
		
		System.out.println("Now the swapped numbers are:");
		System.out.println("a:"+a);
		System.out.println("b:"+b);
		
		
		
		

	}

}
