package week.day1;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Enter the year to be checked:");
		Scanner scan=new Scanner(System.in);
		int year=scan.nextInt();
		
		if(year%4==0&&year%100!=0||year%400==0)
		{
			System.out.println(year+ " is a leap year ");
		}
		
		else
		{
			System.out.println(year+" is not a leap year");
		}
	}

}
