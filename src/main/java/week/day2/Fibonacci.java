package week.day2;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		
		int first=0;
		int second=1;
		int third=0;
		
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the limit");
		int limit=scan.nextInt();
		
		System.out.print("Fibonacci series is  " +first );
		System.out.print("  ");
		System.out.print(second);
		
		for(int i=2;i<=limit;i++)
		{
		
   		third=first+second;
			first=second;
			second=third;
			System.out.print("  ");
			System.out.print(third);
			
		}
			
		}

	}

