package utilreports;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;



public class Reporter {
	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public ExtentTest test;
	public String testcaseName,testDescrip,author,category,sheetName;
		
	
	@BeforeSuite(groups="common")
	public void StartResult() {	
	html=new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	extent=new ExtentReports();
	extent.attachReporter(html);
	
	}
	
	
	@BeforeMethod(groups="common")
	public void StartTestCase() {
		
	test = extent.createTest(testcaseName,testDescrip);
	test.assignAuthor(author);
	test.assignCategory(category);
	
	}
	
	public void reportsteps(String status,String testDescrip) {
		
		
	if(status.equalsIgnoreCase("pass"))	
	{
		test.pass(testDescrip);
		
	}else if(status.equalsIgnoreCase("fail"))
	{
		test.fail(testDescrip);
	}
			
	/*test.pass("Step1:passed",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	test.fail("Step2:failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	test.warning("Step3:warning",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	*/

	
	}
	
	
	
	
	@AfterSuite(groups="common")
	public void StopResult() {
		
		extent.flush();
	}
	
	
	
	

}
