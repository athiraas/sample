package Annotations;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Annotations {
	
	
    @BeforeSuite
	public void GetUp() 
	
	{
		System.out.println("Woke up by 6.00");
		
	}
    
    @BeforeClass
    public void Brush_Up() {
    	
    	System.out.println("Brushed");
    	
    }
	
    @BeforeMethod
    public void Dress_Up() {
    	
    	System.out.println("Wore Formals");
    	
    	
    }
	
    
    @Test
    public void At_Work() {
    	
    	System.out.println("Started doing alloted work");
    }
    
    
    
    @AfterMethod
    public void rest() {
    	
    	System.out.println("Taking rest after work");
    }
    
    
    @AfterClass
    public void Go_Home() {
    	
    	System.out.println("Going home from office");
    	
    }
    
	@AfterSuite
    public void sleep() {
    	
    	System.out.println("Start Sleeping");
    }
	
}
