package ProjectZoomcar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.Get;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Zoomcar {

	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
			
		driver.findElementByXPath("//a[@href='/chennai/search']").click();
		
		WebDriverWait wait=new WebDriverWait(driver,2000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='items'])[3]")));
		
		driver.findElementByXPath("(//div[@class='items'])[3]").click();
		driver.findElementByXPath("//button[@class='proceed' and text()='Next']").click();
		
		Date date=new Date();
		
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);
		
		
		int tomorrow=Integer.parseInt(today)+1;
		
		System.out.println(tomorrow);
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		
		driver.findElementByXPath("//button[@class='proceed' and text()='Next']").click();
	    String selecteddate=driver.findElementByXPath("//div[@class='day picked low-price']").getText();
	    System.out.println(selecteddate);
	    
	    String tomorr=Integer.toString(tomorrow);
	    
	    if(selecteddate.contains(tomorr))
	    {
	    	System.out.println("verification is successful");
	    }else
	    {
	    	System.out.println("verification is not successful");
	    }
	    	
	    driver.findElementByXPath("//button[@class='proceed']").click();	
	    
	    WebElement listedcars = driver.findElementByXPath("//div[@class='car-list-layout']");
	    List<WebElement> cars = listedcars.findElements(By.xpath("//div[@class='car-listing']"));
	    int numofcars = cars.size();
	    System.out.println(numofcars);
	    
	    List<WebElement> pricelist = driver.findElements(By.xpath("//div[@class='price']"));
	    List pricelistof=new ArrayList<>();
	    
	    for (WebElement prices : pricelist) {
			
	    	
	    	String price_list=prices.getText();
	    	price_list=price_list.replaceAll("\\W ","");
	    //	System.out.println(price_list);
	    	int price=Integer.parseInt(price_list);
	    	pricelistof.add(price);
		}
	    
	   Collections.sort(pricelistof);
	   System.out.println(pricelistof);
	    
		int size_1=pricelistof.size()-1;
		Object highestprice = pricelistof.get(size_1);
		System.out.println(highestprice);
		
		   
	   WebElement highpricecar = driver.findElementByXPath("//div[contains(text(),'"+highestprice+"')]//preceding::h3[1]");
	   String carname=highpricecar.getText();
	   System.out.println(carname);
	   driver.findElementByXPath("//div[contains(text(),'"+highestprice+"')]//following::button").click();
	   driver.close();
		

	}

}
