package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.ProjectMethod;
import wdmethods.SeMethods;

public class DuplicateLead extends ProjectMethod {
	
	
	
	@BeforeClass(groups="common")
//	@BeforeClass
	public void setData() {
		
		testcaseName="DuplicateLead";
		testDescrip="Duplicating a new lead in leaftaps";
		author="Athira";
		category="Smoke";
		
	}
	
	
	
		
	
	
//@Test(priority=1)
   @Test(groups="test.sanity")
  
public void duplicate() throws InterruptedException	{
	
	
	WebElement Findlead = locateElement("xpath","//a[@href='/crmsfa/control/findLeads']");
	click(Findlead);
	
	WebElement email = locateElement("linktext","Email");
	clickWithnoSnapshot(email);
	WebElement emailid = locateElement("name","emailAddress");
	type(emailid,"athiraas@in.ibm.com");
	
	WebElement Findleadbutton = locateElement("xpath","//button[text()='Find Leads']");
	clickWithnoSnapshot(Findleadbutton);
	
	Thread.sleep(1500);
	
	WebElement searchresult = locateElement("xpath","(//div[@class='x-panel-body xedit-grid']//child::a)[26]");
	String textresult=getText(searchresult);
	clickWithnoSnapshot(searchresult);
	
	System.out.println(driver.getTitle());
	
	verifyTitle("Duplicate lead | opentaps CRM");
	
	WebElement duplicate = locateElement("linktext","Duplicate Lead");
	clickWithnoSnapshot(duplicate);
	
	WebElement CreateLead = locateElement("xpath","//input[@value='Create Lead']");
	clickWithnoSnapshot(CreateLead);
	
	WebElement Verifyname = locateElement("xpath","//span[@id='viewLead_firstName_sp']");
	if(Verifyname.getText().equals(textresult))
	{
		System.out.println("Your Duplicate lead verified successfully");
		
	}else
		System.out.println("Your duplicate lead is not successful");
		
		
//	closeBrowser();	
		
		
		
		
	
	
	
	
	
}
	
	
	
	
	
	
}
