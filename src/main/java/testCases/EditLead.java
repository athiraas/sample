package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.ProjectMethod;
import wdmethods.SeMethods;

public class EditLead extends ProjectMethod {
	
	
	@BeforeClass(groups="common")
	public void setData() {
		
		testcaseName="EditLead";
		testDescrip="Editing a new lead in leaftaps";
		author="Athira";
		category="Smoke";
		
	}
	
	
	
   
	
	
//@Test(priority=4)
@Test(groups="test.regression")  
public void Edit() throws InterruptedException {
	

	
	WebElement Findlead = locateElement("xpath","//a[@href='/crmsfa/control/findLeads']");
	click(Findlead);
	
	WebElement Firstname=locateElement("xpath","(//input[@name='firstName'])[3]");
	type(Firstname,"athira");
	
	WebElement Findleadbutton = locateElement("xpath","//button[text()='Find Leads']");
	click(Findleadbutton);
	
	WebElement searchresult = locateElement("xpath","(//div[@class='x-panel-body xedit-grid']//child::a)[24]");
	clickWithnoSnapshot(searchresult);
	
	System.out.println(driver.getTitle());
	
	verifyTitle("View lead | opentaps CRM");
	
	
	WebElement edit = locateElement("linktext","Edit");
	clickWithnoSnapshot(edit);
	
	Thread.sleep(2000);
	
	WebElement cmpnyname = locateElement("id", "updateLeadForm_companyName");
	type(cmpnyname, "Accenture");
	
	WebElement Update = locateElement("xpath","//input[@value='Update']");
	click(Update);
	
	WebElement verifyupdate=locateElement("xpath","//span[@id='viewLead_companyName_sp']");
	if(getText(verifyupdate).contains("Accenture"))
	{
		System.out.println("Your update is successfully verified");
	}else
	{
		System.out.println("Your update is not successfully");	
	}
			
//	closeBrowser();
	
	
	
}






}
