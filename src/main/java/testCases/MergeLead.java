package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.ProjectMethod;
import wdmethods.SeMethods;

public class MergeLead extends ProjectMethod {
	
	
	
	@BeforeClass(groups="common")
	public void setData() {
		
		testcaseName="MergeLead";
		testDescrip="Merging a new lead in leaftaps";
		author="Athira";
		category="Smoke";
		
	}
	
//	@Test(priority=2)
    @Test
    //(groups="test.smoke")   
	public void Merge() throws InterruptedException {
		
	
		WebElement MergeLeads = locateElement("linktext","Merge Leads");
		click(MergeLeads);
		
		WebElement image1 = locateElement("xpath","//table[@id='widget_ComboBox_partyIdFrom']/following::img[1]");
		clickWithnoSnapshot(image1);
		
		switchToWindow(1);
			
		WebElement Mergenum = locateElement("xpath","//input[@name='id']");
		type(Mergenum,"10051");
		
		WebElement findlead = locateElement("xpath","//button[text()='Find Leads']");
		clickWithnoSnapshot(findlead);
		Thread.sleep(2000);
		
		WebElement fromfind = locateElement("xpath","(//div[@class='x-panel-body xedit-grid']//child::a)[24]");
		clickWithnoSnapshot(fromfind);
		
		switchToWindow(0);
		
		WebElement image2 = locateElement("xpath","//table[@id='widget_ComboBox_partyIdFrom']/following::img[2]");
		clickWithnoSnapshot(image2);
		
		switchToWindow(1);
		
		WebElement Mergenum1 = locateElement("xpath","//input[@name='id']");
		type(Mergenum1,"10055");
		
		WebElement findlead1 = locateElement("xpath","//button[text()='Find Leads']");
		clickWithnoSnapshot(findlead1);
		
		Thread.sleep(2000);
		
		WebElement tofind = locateElement("xpath","(//div[@class='x-panel-body xedit-grid']//child::a)[24]");
		clickWithnoSnapshot(tofind);
		
		switchToWindow(0);
		
		WebElement Mergebutton = locateElement("linktext","Merge");
		clickWithnoSnapshot(Mergebutton);
		
		acceptAlert();
		
		WebElement findleadlink = locateElement("xpath","//a[@href='/crmsfa/control/findLeads']");
		clickWithnoSnapshot(findleadlink);
		
		WebElement Mergenum2 = locateElement("xpath","//input[@name='id']");
		type(Mergenum2,"10225");
		
		WebElement findlead2 = locateElement("xpath","//button[text()='Find Leads']");
		clickWithnoSnapshot(findlead2);
		
		WebElement verify = locateElement("xpath","//div[text()='No records to display']");
		System.out.println(verify.getText());
		
		if(verify.getText().contains("No records to display")) {
			System.out.println("Your Merge is successfully verified");}
			else 
				System.out.println("Your verify failed");
		
		
//	closeBrowser();	
		
		
		
		
	}
	
	
	

}
