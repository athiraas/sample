package testCases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import wdmethods.ProjectMethod;
import wdmethods.SeMethods;

public class DeleteLead extends ProjectMethod {
	
	
	@BeforeClass(groups="common")
//	@BeforeClass
	public void setData() {
		
		testcaseName="DeleteLead";
		testDescrip="deleting a new lead in leaftaps";
		author="Athira";
		category="Smoke";
		
	}
	
	
	
	
	
	
	//@Test(priority=3)
	@Test(groups="test.smoke") 
	public void delete() throws InterruptedException {
		
		
		
		WebElement Mainlead = locateElement("xpath","//a[@href='/crmsfa/control/leadsMain']");
		click(Mainlead);
		
		WebElement Findlead = locateElement("xpath","//a[@href='/crmsfa/control/findLeads']");
		click(Findlead);
		
		WebElement phone = locateElement("xpath","//span[text()='Phone']");
		click(phone);
	
		WebElement phoneNumber = locateElement("name","phoneNumber");
		type(phoneNumber,"9995346443");
	
		WebElement find = locateElement("xpath","//button[@class='x-btn-text' and text()='Find Leads']");
		click(find);
		
		Thread.sleep(200);
		
		WebElement leadid = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		String textresult=getText(leadid);
		
		clickWithnoSnapshot(leadid);
		
		WebElement Delete = locateElement("linktext","Delete");
		clickWithnoSnapshot(Delete);
		
		/*WebDriverWait wait=new WebDriverWait(driver, 500);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='/crmsfa/control/findLeads']")));
		*/
		
		Thread.sleep(500);
		WebElement Findlead1 = locateElement("xpath","//a[@href='/crmsfa/control/findLeads']");
		click(Findlead1);
		
	//	clickWithnoSnapshot(Findlead);
		
		WebElement name = locateElement("name", "id");
		type(name,textresult);
		
		WebElement find1 = locateElement("xpath","//button[@class='x-btn-text' and text()='Find Leads']");
		click(find1);
		/*Thread.sleep(25000);
		click(find);
		*/
		
		WebElement verify = locateElement("xpath","//div[text()='No records to display']");
		System.out.println(verify.getText());
		
		if(verify.getText().contains("No records to display")) {
			System.out.println("Your delete is successfully verified");}
			else 
				System.out.println("Your verify failed");
		
		
//	closeBrowser();	
				
		}
		
		
	}
	
	
	
	
	


