package testCases;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethod;
import wdmethods.SeMethods;
import week6.day2.Create_LearnExcel;




public class TC001_Login extends ProjectMethod{
	
	@BeforeClass(groups="common")
//	@BeforeClass
	public void setData() {
		testcaseName="TC001_Login";
		testDescrip="create a new lead in leaftaps";
		author="Athira";
		category="Smoke";
		sheetName="createlead";
		
	}
	

/*	@BeforeMethod
	public void logintobrowser(){
	
		login();	
	}
  
	*/

//	@Test(invocationCount=2,invocationTimeOut=20000,priority=5)
//  @Test(groups="test.regression")
  
   
   
   
   
   
   
   
   
  
   
   
   
   
   
  /* public String[][] getData(){
	   
	   
		   String[][] data=new String[2][3];
		   data[0][0]="TL";
		   data[0][1]="Athira";
		   data[0][2]="A.S";
		   data[1][0]="TL";
		   data[1][1]="Clinda";
		   data[1][2]="Thomas";
		   	   
	   
	return data;
	     
	   
   }*/
   
//	@Test(invocationCount=2,invocationTimeOut=20000,priority=5)
 // @Test(groups="test.regression")
   
   @Test(dataProvider="fetchdata",groups="test.regression")	
	public void createLead(String Cname,String Fname,String lname,String emai) throws InterruptedException {
		
		WebElement eleLead = locateElement("linktext","Leads");
		click(eleLead);
	    WebElement eleCreateLead = locateElement("linktext","Create Lead");
		click(eleCreateLead);
		System.out.println("Cname:"+Cname);
		WebElement cmpnyname = locateElement("id", "createLeadForm_companyName");
		type(cmpnyname, Cname); 
		System.out.println("Fname:"+Fname);
		WebElement Firstname = locateElement("id", "createLeadForm_firstName");
		type(Firstname,Fname);	
		System.out.println("lname:"+lname);
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname,lname);
		System.out.println("emai:"+emai);
		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		type(email,emai);
		
/*		WebElement Parentacc = locateElement("xpath","//input[@id='createLeadForm_parentPartyId']/following::img[1]");
		clickWithnoSnapshot(Parentacc);
		
		switchToWindow(1);
		
		WebElement AccountID = locateElement("xpath", "//input[@class=' x-form-text x-form-field']");
		type(AccountID, "Company");
		 
		WebElement AccountName = locateElement("xpath", "//input[@name='accountName']");
		type(AccountName, "Your Company Name");
		
		WebElement FindAccount = locateElement("xpath", "//button[text()='Find Accounts']");
		click(FindAccount); 
		
		Thread.sleep(2000);
		WebElement Findresult=locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a)[1]");
		clickWithnoSnapshot(Findresult);
		
		switchToWindow(0);
		
		WebElement Firstname = locateElement("id", "createLeadForm_firstName");
		type(Firstname, "Athira");
		
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, "A S");
		
		WebElement Source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(Source,"Employee","visible");
		
		WebElement CampaignId = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(CampaignId,"CATRQ_CARNDRIVER","value");
		
				
		WebElement Firstnamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(Firstnamelocal, "Athi");
		
		WebElement lastnamelocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(lastnamelocal, "AS");
		
		WebElement Title = locateElement("id", "createLeadForm_personalTitle");
		type(Title, "Welcome");
		
		WebElement Calender = locateElement("id", "createLeadForm_birthDate-button");
		clickWithnoSnapshot(Calender);
		
		String mydateofbirth="04/27/1993";
		
		//retrive month and date
		String[] montharray=mydateofbirth.split("/");
		int monthtoselect=Integer.parseInt(montharray[0]);
		int yeartoselect=Integer.parseInt(montharray[2]);
		
		
		
		//finding the month of the calendar
       WebElement monthcolumn=locateElement("xpath","//div[@class='calendar']/table/thead/tr[1]/td[2]");
		String text=monthcolumn.getText();
		String[] textmontharray=text.split(", ");

		String mymonth=textmontharray[0];
		int currentyear=Integer.parseInt(textmontharray[1]);
		System.out.println(currentyear);
		
		//Set the format of calendar to month test
		SimpleDateFormat inputFormat=new SimpleDateFormat("MMMM");
		Calendar cal=Calendar.getInstance();
		cal.setTime(inputFormat.parse(mymonth));
		
		//converting month name to month number
		
		SimpleDateFormat outputFormat=new SimpleDateFormat("MM");
		System.out.println(outputFormat.format(cal.getTime()));
		int presentmonth=Integer.parseInt(outputFormat.format(cal.getTime()));
		System.out.println(presentmonth);
		
		if(monthtoselect<presentmonth)
		{
		 
			
		for (int i = 0; i <presentmonth-monthtoselect; i++) {
			WebElement monthbutton = locateElement("xpath", "//tr[@class='headrow']/child::td[2]");
		click(monthbutton);
			Thread.sleep(200);
		}
		}
		
		
		if(yeartoselect<currentyear)
		{
			for(int i=0;i<currentyear-yeartoselect;i++)
			{
				
				WebElement yearbutton = locateElement("xpath", "//tr[@class='headrow']/child::td[1]");
				click(yearbutton);
				Thread.sleep(500);					
			}
				
		}
			
		
		WebElement date = locateElement("xpath", "//td[text()='27']");
		clickWithnoSnapshot(date);

		WebElement proftitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(proftitle, "Testleaf");
		
		WebElement Depname = locateElement("id", "createLeadForm_departmentName");
		type(Depname, "Mainframe");
		
		WebElement Revenue = locateElement("id", "createLeadForm_annualRevenue");
		type(Revenue, "500000");
		
		
		WebElement curreny = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(curreny,"AOK - Angolan Kwanza","visible");
		
		WebElement industry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(industry,"Computer Software","visible");
	
			
		WebElement Noemployee = locateElement("id", "createLeadForm_numberEmployees");
		type(Noemployee, "10");
		
		WebElement ownership = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownership,"Partnership","visible");
		
		
		
		WebElement Siscode = locateElement("id", "createLeadForm_sicCode");
		type(Siscode, "Clinda");
		
		WebElement tickersymbo = locateElement("id", "createLeadForm_tickerSymbol");
		type(tickersymbo, "yes");
		
		WebElement Description = locateElement("id", "createLeadForm_description");
		type(Description, "Testleaf is good firm");
		
		WebElement Note = locateElement("id", "createLeadForm_importantNote");
		type(Note, "Learning makes you better person");
		
		WebElement Contrycode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(Contrycode, "+91");
		
		WebElement phonearea = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(phonearea, "91");
		
		WebElement phone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phone, "9995346553");
		
		
		WebElement phoneexts = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(phoneexts, "+91");
		
		WebElement Phoneask = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(Phoneask, "Athira");
		
		
		
		WebElement Weburl = locateElement("id", "createLeadForm_primaryWebUrl");
		type(Weburl, "www.athira.com");
		
		WebElement genname = locateElement("id", "createLeadForm_generalToName");
		type(genname, "Smigith");
		
		WebElement genattname = locateElement("id", "createLeadForm_generalAttnName");
		type(genattname, "smiju");
		
		WebElement Adress = locateElement("id", "createLeadForm_generalAddress1");
		type(Adress, "Anedath");
		
		WebElement Adress2 = locateElement("id", "createLeadForm_generalAddress2");
		type(Adress2, "peruvalloor");
		
		WebElement city = locateElement("id", "createLeadForm_generalCity");
		type(city, "thrissur");
		
		WebElement genpost = locateElement("id", "createLeadForm_generalPostalCode");
		type(genpost, "680508");
		
		WebElement Country = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(Country,"India","visible");
		
		Thread.sleep(1200);
		WebDriverWait wait1=new WebDriverWait(driver,500);
		wait1.until(ExpectedConditions.textToBePresentInElementLocated(By.id("createLeadForm_generalStateProvinceGeoId"), "KERALA"));
		
		WebElement State = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(State,"KERALA","visible");
						
		
		WebElement postalcode = locateElement("id", "createLeadForm_generalPostalCodeExt");
		type(postalcode, "680508");
*/		
		driver.findElementByClassName("smallSubmit").click();
		
		
		
		
		
			
		
	}
}









