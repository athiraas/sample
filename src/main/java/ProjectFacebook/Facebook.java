package ProjectFacebook;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Facebook {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeOptions op=new ChromeOptions();
		op.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(op);
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(2000,TimeUnit.SECONDS);
		
		WebElement emailid = driver.findElementById("email");
		emailid.sendKeys("athirasatheesan25@gmail.com");
		WebElement password = driver.findElementById("pass");
		password.sendKeys("Athira@123");
		driver.findElementByXPath("//input[@value='Log In']").click();
		
	
		WebElement searchbar = driver.findElementByXPath("//input[@name='q']");
		searchbar.sendKeys("Testleaf");
	
		driver.findElementByXPath("//button[@data-testid='facebar_search_button' and @type='submit']").click();
		Thread.sleep(2000);
		
		
		WebDriverWait driverWait = new WebDriverWait(driver,500);
		driverWait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("(//span[text()='Places']//following::button)[1]"),"Like"));
		
		
		WebElement likebutton = driver.findElementByXPath("(//span[text()='Places']//following::button)[1]");
		String textinlike = likebutton.getText();
		System.out.println(textinlike);
		
		
		
		if(textinlike.equals("Like"))
		{
			
			likebutton.click();
		}else if(textinlike.equals("Liked"))
		{
			System.out.println("The button is already liked");
		}
		
		WebElement Testleaf = driver.findElementByXPath("//span[text()='Places']//following::div[8]");
		Testleaf.click();
		
		System.out.println(driver.getTitle());
		if(driver.getTitle().contains("Testleaf"))
		{
			System.out.println("Yes the title contains the text Testleaf");
		}else
			System.out.println("Title do not contain Testleaf");
		
		String text = driver.findElementByXPath("(//div[@class='clearfix _ikh'])[2]").getText();
		System.out.println(text);
		
		driver.close();

}

}