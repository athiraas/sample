package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.Create_LearnExcel;



public class ProjectMethod extends SeMethods{
	
	 @DataProvider(name="fetchdata")
	   
	   public String[][] getfromExecl() throws IOException{
		   
		 return  Create_LearnExcel.getData(sheetName);
		    
		   
	   }
	   		
	
	
@Parameters({"browser","url","username","password"})
//@BeforeMethod(groups="common")
@BeforeMethod

public void login(String browser,String url,String uname,String passwr) {
startApp("chrome","http://leaftaps.com/opentaps/");
//startApp(browser,url);
WebElement eleUsername = locateElement("id", "username");
type(eleUsername,uname);
WebElement elePassword = locateElement("id","password");
type(elePassword,passwr);
WebElement eleLogin = locateElement("class", "decorativeSubmit");
click(eleLogin); 
WebElement eleCRM = locateElement("linktext","CRM/SFA");
click(eleCRM);
WebElement Leads = locateElement("linktext","Leads");
click(Leads);
	
}

 @AfterMethod(groups="common")	   
   public void tearDown() {
	   closeBrowser();
   }


	
}
