package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utilreports.Reporter;

public class SeMethods extends Reporter implements WdMethods {
	public RemoteWebDriver driver;
	int i =1;
	@Override
	public void startApp(String browser, String url)  {
		
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			}else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.get(url); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The browser "+browser+" launched successfully"); 
			takeSnap();
		
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "name": return driver.findElementByName(locValue);	
			case "xpath": return driver.findElementByXPath(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "linktext": return driver.findElementByLinkText(locValue);
			case "tagname" :return driver.findElementByTagName(locValue);
			case "cssvalue": return driver.findElementByCssSelector(locValue);
			case "partiallinktext":return driver.findElementByPartialLinkText(locValue);
							
			}
		} catch (NoSuchElementException e) {
			System.out.println("The element is not found");
		} catch (WebDriverException e) {
			System.out.println("Unknown Exception occured");
		} 
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		
		try {
			if(locValue=="id")
			return driver.findElementById(locValue);
			
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.out.println("The element is not found");
		}
		return null;
	}

	@Override
	public void type(WebElement ele, String data)  {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportsteps("pass","The data " +data+ "enter successfully");
			//System.out.println("The data "+data+" enter successfully");
			
			
		} catch (WebDriverException e) {
			reportsteps("fail","The data " +data+ "not enter successfully");
			
		//System.out.println("The data "+data+" not enter successfully");
		} finally {
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked"); 
		takeSnap();
	}
	
	@Override
	public void clickWithnoSnapshot(WebElement ele)
	{
		ele.click();
		System.out.println("the element "+ele+ "clicked");
	}
	
	

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		System.out.println("the text is "+text );
		return text;
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value, String sel) {
		
         try {
			Select dd=new Select(ele);
    
    if(sel.equalsIgnoreCase("visible")) {
			 dd.selectByVisibleText(value);	
			 
    }else if(sel.equalsIgnoreCase("value")) {
			 dd.selectByValue(value);
			 
    }else if(sel.equalsIgnoreCase("index")) {
			 dd.selectByIndex(Integer.parseInt(value));
    }
		} catch (WebDriverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     	
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		
		try {
			Select dd=new Select(ele);
			dd.selectByIndex(index);
			System.out.println("Dropdown using index"+index+"selected");
			
		} catch (IndexOutOfBoundsException e) {
			
			System.out.println("Index is not correct");
		}
	    	 
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean bReturn=false;
		String title=driver.getTitle();
		if(title.equals(expectedTitle)) {
			System.out.println("The title is matched with "+expectedTitle);
			bReturn=true;
		}
			else
			{
				System.out.println("The title is not matched with "+expectedTitle);	
			}
				
		return bReturn;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
		if(ele.getText().equals(expectedText)) {
			System.out.println("The text matched");
		}else
		{
			System.out.println("The text not matched");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		if(ele.getText().contains(expectedText)) {
			System.out.println("The text partially matched");
		}else
		{
			System.out.println("The text does not contain"+expectedText);
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(attribute).equals(value))
		{
			System.out.println("The attribute text matched");
		}else
		{
			System.out.println("The attribute text not matched");
		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
	
		if(ele.getAttribute(attribute).contains(value))
		{
			System.out.println("The attribute text matched");
		}else
		{
			System.out.println("The attribute text not matched");
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
	
	try {
		Set<String> Uniquenum = driver.getWindowHandles();
		List<String> Uninum = new ArrayList<>();
		Uninum.addAll(Uniquenum);
		driver.switchTo().window(Uninum.get(index));
	} catch (NoSuchWindowException e) {
		
		System.out.println("Window not present");
		
	}
			
			
	}

	@Override
	public void switchToFrame(WebElement ele,String id,String name,String sel) {
		
		if(sel.equalsIgnoreCase("id")) {
			driver.switchTo().frame(id);
			
		}else if(sel.equalsIgnoreCase("name")) {
			driver.switchTo().frame(name);
		}else if(sel.equalsIgnoreCase("WebElement")) {
			driver.switchTo().frame(ele);
		}else if(sel.equalsIgnoreCase("index")) {
			driver.switchTo().frame(Integer.parseInt(name));
		}
			
	  	
		
	
	}

	@Override
	public void acceptAlert() {
		
		try {
			
			driver.switchTo().alert().accept();
			System.out.println("Alert accepted");
			
			
		}catch(NoAlertPresentException e){
			
			System.out.println("Alert not present");
		}
			
		}

	@Override
	public void dismissAlert() {
		
      try {
			driver.switchTo().alert().dismiss();;
			System.out.println("Alert dismissed");
			
			
		}catch(NoAlertPresentException e){
			
			System.out.println("Alert not present");
		}

	}

	@Override
	public String getAlertText() {
		
	String text=driver.switchTo().alert().getText();
	return text;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
	//	test.addScreenCaptureFromPath("./../snaps/img1.png");
		try {
			FileUtils.copyFile(src, dec);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		
		driver.quit();

	}

}
		
		
	


