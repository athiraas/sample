package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import wdmethods.ProjectMethod;

public class Create_LearnExcel {

	 public static String[][] getData(String sheetname) throws IOException {
			
			XSSFWorkbook myBook = new XSSFWorkbook("./data/"+sheetname+".xlsx");
			XSSFSheet sheet= myBook.getSheet(sheetname);
			int rowcount = sheet.getLastRowNum();
			System.out.println("rowcount is"+rowcount);
			
		//	int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
			
			int cellcount = sheet.getRow(0).getLastCellNum();
			System.out.println("column count is"+cellcount);
			String[][] data=new String[rowcount][cellcount];
			
			for(int i=1;i<=rowcount;i++) {
				
				XSSFRow row = sheet.getRow(i);
				for(int j=0;j<cellcount;j++)
				{
					XSSFCell cell = row.getCell(j);
					String cellValue = cell.getStringCellValue();
					System.out.println(cellValue);
					data[i-1][j] = cellValue;
				}
			}
			return data;
	   }
		
		
		
		
		
		
		
		

	}


