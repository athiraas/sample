package week3.day1;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginandCreateLead {

	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.manage().window().maximize();
		
		WebElement eleUsername = driver.findElementById("username");
		eleUsername.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
        driver.findElementByLinkText("CRM/SFA").click();
        driver.findElementByLinkText("Create Lead").click();
        driver.findElementById("createLeadForm_companyName").sendKeys("Testleaf");
        driver.findElementById("createLeadForm_firstName").sendKeys("Athira");
        driver.findElementById("createLeadForm_lastName").sendKeys("A S");
        
        WebElement source=driver.findElementById("createLeadForm_dataSourceId");
        Select se=new Select(source);
        se.selectByVisibleText("Employee"); 
       
        WebElement source1=driver.findElementById("createLeadForm_marketingCampaignId");
        Select se1=new Select(source1);
        se1.selectByValue("CATRQ_CARNDRIVER");
        
        driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Athi");
        driver.findElementById("createLeadForm_lastNameLocal").sendKeys("AS");
        driver.findElementById("createLeadForm_personalTitle").sendKeys("Welcome");
        driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Testleaf");
        driver.findElementById("createLeadForm_departmentName").sendKeys("Mainframe");
        driver.findElementById("createLeadForm_annualRevenue").sendKeys("5000000");
        
        
        WebElement source3=driver.findElementById("createLeadForm_currencyUomId");
        Select se3=new Select(source3);
        se3.selectByVisibleText("AOK - Angolan Kwanza");
        
        WebElement source2=driver.findElementById("createLeadForm_industryEnumId");
        Select se2=new Select(source2);
        se2.selectByVisibleText("Computer Software");
        
        
       /* List<WebElement> allOptions=se2.getOptions();
        for (WebElement eachOptions : allOptions) {
        	if(eachOptions.getText().startsWith("C"))
			{
		System.out.println(eachOptions.getText());
			}
     	   */
        
        
        
        
        driver.findElementById("createLeadForm_numberEmployees").sendKeys("10");   
        
        WebElement source4=driver.findElementById("createLeadForm_ownershipEnumId");
        Select se4=new Select(source4);
        se4.selectByVisibleText("Partnership");
        
        driver.findElementById("createLeadForm_sicCode").sendKeys("Clinda");
        driver.findElementById("createLeadForm_tickerSymbol").sendKeys("yes");
        driver.findElementById("createLeadForm_description").sendKeys("Testleaf is good firm");
        driver.findElementById("createLeadForm_importantNote").sendKeys("Learning makes you better person");
        driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("IN");
        
        driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("680508");
        driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9995346553");
        driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("+91");
        driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("athira");
        driver.findElementById("createLeadForm_primaryEmail").sendKeys("athirasatheesan25@gmail.com");
        driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.athira.com");
        driver.findElementById("createLeadForm_generalToName").sendKeys("smigith");
        driver.findElementById("createLeadForm_generalAttnName").sendKeys("Ariyilla");
        driver.findElementById("createLeadForm_generalAddress1").sendKeys("Anedath");
        driver.findElementById("createLeadForm_generalAddress2").sendKeys("Peruvalloor");   
        
        driver.findElementById("createLeadForm_generalCity").sendKeys("Thrissur");
        
        driver.findElementById("createLeadForm_generalPostalCode").sendKeys("680508");
        
        WebElement source5=driver.findElementById("createLeadForm_generalCountryGeoId");
        Select se5=new Select(source5);
        se5.selectByVisibleText("India");
        
        WebDriverWait wait=new WebDriverWait(driver,500);
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("createLeadForm_generalStateProvinceGeoId"),"KERALA"));
        
        
        WebElement source7=driver.findElementById("createLeadForm_generalStateProvinceGeoId");
        Select se7=new Select(source7);
        se7.selectByVisibleText("KERALA");
        
        List<WebElement> elements=se7.getOptions();
        System.out.println(elements.size());
        
        for(int i=0;i<elements.size();i++)
        {
        	String str=elements.get(i).getText();
        	System.out.println(str);
        	
        }
       
        
        driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("680508"); 
        
        driver.findElementByClassName("smallSubmit").click();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       
              
        
       
       
             
      
       
      
    	   
    
    	   
		
				
	}
       
       
       
       
       
       
       
       
       
	}


