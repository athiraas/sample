package week3.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestleafDelete {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.manage().window().maximize();
		
		WebElement eleUsername = driver.findElementById("username");
		eleUsername.sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
        driver.findElementByLinkText("CRM/SFA").click();
        driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
        driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
        driver.findElementByXPath("//span[text()='Phone']").click();
        driver.findElementByName("phoneNumber").sendKeys("7373404766");
        driver.findElementByXPath("//button[@class='x-btn-text' and text()='Find Leads']").click();
        
        Thread.sleep(2000);
        String str=driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a").getText();
        driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a").click();
        
        driver.findElementByLinkText("Delete").click();
        driver.findElementByXPath("//a[@href='/crmsfa/control/findLeads']").click();
        driver.findElementByName("id").sendKeys(str);
        driver.findElementByXPath("//button[@class='x-btn-text' and text()='Find Leads']").click();
        
        
        
        driver.close();
        
        
        
        
        
        
        
        
        
        
        
        
        
	}

}
