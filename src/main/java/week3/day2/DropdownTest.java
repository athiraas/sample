package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropdownTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		
		
		WebElement Element=driver.findElementById("dropdown1");
		
		Select dropoptions=new Select(Element);
		
		List<WebElement> options=dropoptions.getOptions();
		int count=options.size();
		
		int i=0;
		for (WebElement Options2 : options) {
			
			i++;
			
			if(i==count)
			{
				Options2.getText();
				Options2.click();
			}
			
		}
		
	}

}
