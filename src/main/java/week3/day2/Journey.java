package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.formula.functions.Rows;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Journey {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://erail.in/");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    driver.findElementById("txtStationFrom").clear();
	    driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
	    driver.findElementById("txtStationTo").clear();
	    driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
	    WebElement check = driver.findElementById("chkSelectDateOnly");
	    
	    if(check.isSelected())
	    {
	    	check.click();
	    }
	    
	    driver.findElementByXPath("//a[@title='Click here to sort on Train Name']").click();
	    
	    
	    WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
	    List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());

		
		for (int i = 0; i <rows.size(); i++) {
			WebElement eachRow = rows.get(i);
			List<WebElement> allCell = eachRow.findElements(By.tagName("td"));
			System.out.println(allCell.get(1).getText());
		}
		
		
		
	}

}
