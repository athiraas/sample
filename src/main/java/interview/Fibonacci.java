package interview;

public class Fibonacci {

	public static void main(String[] args) {
		
		System.out.println("Fibonacci series is:");
		
		int first=1;
		int second=1;
		int sum=0;
		
		System.out.print("1,1 ");
		for(int i=0;i<10;i++)
		{
			sum=first+second;
			first=second;
			second=sum;
			System.out.print(","+sum);	
			
		}
	}

}
